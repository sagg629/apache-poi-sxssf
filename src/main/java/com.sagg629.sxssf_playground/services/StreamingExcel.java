package com.sagg629.sxssf_playground.services;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.awt.Color;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * SXSSF Streaming Usermodel API (http://poi.apache.org/spreadsheet/how-to.html#sxssf)
 * SXSSF (package: org.apache.poi.xssf.streaming) is an API-compatible streaming extension of XSSF
 * to be used when very large spreadsheets have to be produced, and heap space is limited.
 * Sheet: default column width: 8 (56.0136 px)
 * -
 * Benchmarks (keeping 10 rows in memory):
 * sheets:  1, rows: 10k, cols: 5, time: ~200ms
 * sheets:  1, rows: 10k, cols: 16, time: ~500ms
 * sheets:  1, rows: 10k, cols: 50, time: ~1.6s - ~2.2s
 * sheets: 10, rows: 10k, cols: 16, time: ~4.22s
 * sheets:  1, rows: 580, cols: 16, time: ~60ms - ~120ms
 * sheets:  1, rows: 50k, cols: 16, time: ~2.2s
 * -
 * Created by Sergio A Guzman <sagg629@gmail.com> on 8/11/17 11:05 PM.
 */
@RestController
@RequestMapping("services/sxssf")
public class StreamingExcel {
    private static final Logger log = Logger.getLogger(String.valueOf(StreamingExcel.class));
    private static final String FONT_NAME = "Helvetica";
    private static Integer FONT_SIZE = 11;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity writeExcel() throws IOException {
        Map<String, Object> responseMap = doReport();
        runGC();
        return new ResponseEntity<>(responseMap, HttpStatus.OK);
    }

    private Map<String, Object> doReport() throws IOException {
        long timeStartedMillis = System.currentTimeMillis();
        int sheets2Print = 1;
        int rowsPerSheet = 10000;
        int columnsPerRow = 16;
        SXSSFWorkbook wb = new SXSSFWorkbook(10); //keep 10 rows in memory, exceeding rows will be flushed to disk

        //create font
        //... pareciera que con estilo el tiempo para generar es el mismo ... si mucho 50ms o menos
        XSSFFont fontNormal = createFont(wb, FONT_NAME, FONT_SIZE, false, "");
        XSSFCellStyle styleDefault = createCellStyle(wb, fontNormal, "");

        for (int shNum = 0; shNum < sheets2Print; shNum++) {
            //create some sheets
            Sheet sheet = wb.createSheet(String.format("sheet_%s", (shNum + 1)));
            //sample texts
            String text0 = "sergio a guzman ... alias chamo/chemo/checho/frijol/padre";
            String text1 = "alexis corrales";
            String text2 = "lolo";
            String text3 = "luz karime londoño prado";
            String textOthers = "scarabab";

            for (int rowNum = 0; rowNum < rowsPerSheet; rowNum++) {
                //create row
                Row row = sheet.createRow(rowNum);
                for (int cellNum = 0; cellNum < columnsPerRow; cellNum++) {
                    //create a cell for this row
                    Cell cell = row.createCell(cellNum);
                    cell.setCellStyle(styleDefault);

                    switch (cellNum) {
                        case 0:
                            cell.setCellValue(text0);
                            break;
                        case 1:
                            cell.setCellValue(text1);
                            break;
                        case 2:
                            cell.setCellValue(text2);
                            break;
                        case 3:
                            cell.setCellValue(text3);
                            break;
                        default:
                            cell.setCellValue(textOthers);
                            break;
                    }
                }
            }

            //https://stackoverflow.com/a/19007294/3135458)
            sheet.setColumnWidth(0, (text0.length() * 256));
            sheet.setColumnWidth(1, (text1.length() * 256));
            sheet.setColumnWidth(2, (text2.length() * 256));
            sheet.setColumnWidth(3, (text3.length() * 256));
            sheet.setColumnWidth(4, (textOthers.length() * 256));
            sheet.setColumnWidth(5, (textOthers.length() * 256));
        }

        //document --> disk
        FileOutputStream fileOutputStream = new FileOutputStream("test_1.xlsx");
        wb.write(fileOutputStream);
        fileOutputStream.close();

        //dispose of temp files backing this workbook on disk
        wb.dispose();

        //return and exit
        Map<String, Object> responseMap = new LinkedHashMap<>();
        responseMap.put("sheets", sheets2Print);
        responseMap.put("rows", rowsPerSheet);
        responseMap.put("columns", columnsPerRow);
        long timeFinishedMillis = System.currentTimeMillis();
        long elapsedTimeMillis = (timeFinishedMillis - timeStartedMillis);
        log.info(String.format("Sheets: %s - Rows: %s - Cols: %s - Time: %sms", sheets2Print, rowsPerSheet, columnsPerRow, elapsedTimeMillis));
        return responseMap;
    }

    /**
     * Convert a color in HEX "#FFFFFF" to a awt's Color (http://stackoverflow.com/a/4129692/3135458)
     *
     * @param colorStr color in HEX
     * @return java.awt.Color element
     */
    private static Color hex2Rgb(String colorStr) {
        return new Color(
                Integer.valueOf((colorStr.trim()).substring(1, 3), 16),
                Integer.valueOf((colorStr.trim()).substring(3, 5), 16),
                Integer.valueOf((colorStr.trim()).substring(5, 7), 16));
    }

    /**
     * Create parameterizable font for a workbook / document
     *
     * @param workbook     workbook / document
     * @param fontName     font name (e.g. "Helvetica")
     * @param fontSize     font size (e.g. 12)
     * @param bold         font is bold or not
     * @param fontHEXColor font color in HEX format, black by default
     * @return XSSFFont element
     */
    private static XSSFFont createFont(SXSSFWorkbook workbook, String fontName, int fontSize, boolean bold, String fontHEXColor) {
        XSSFFont font = (XSSFFont) workbook.createFont();
        font.setFontName(fontName);
        font.setFontHeight(fontSize);
        font.setBold(bold);
        font.setColor(new XSSFColor(hex2Rgb(fontHEXColor.isEmpty() ? "#000000" : fontHEXColor)));
        return font;
    }

    private static XSSFCellStyle createCellStyle(SXSSFWorkbook workbook, XSSFFont font, String bgHexColor) {
        XSSFCellStyle style = (XSSFCellStyle) workbook.createCellStyle();
        style.setWrapText(false);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        //verify alignment of number-only cells (Precision's line 304 in XLSX file)
        style.setFont(font);
        if (!bgHexColor.isEmpty()) {
            XSSFColor color = new XSSFColor(hex2Rgb(bgHexColor));
            style.setFillForegroundColor(color);
            style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        }
        return style;
    }

    /**
     * Force Garbage Colector to run
     */
    public void runGC() {
        Object object = new Object();
        final WeakReference<Object> ref = new WeakReference<>(object);
        object = null;
        while (ref.get() != null) {
            System.gc();
        }
    }
}
